//
//  UserSavingUpdatingTests.swift
//  BloggingTests
//
//  Created by Nissi Vieira Miranda on 13/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import XCTest
import Fakery
@testable import Blogging

class UserSavingUpdatingTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSaveUpdateUser() {
        
        let user = UserCreator.create()
        XCTAssertTrue(UserSaver.saveUser(user))
        
        if let result = UserFetcher.fetchUserWithId(user.userId) {
            XCTAssertTrue(result.count == 1)
        }
        else {
            XCTFail()
        }
        
        let faker = Faker(locale: "")
        
        XCTAssertTrue(UserUpdater.updateUser(user, withName: faker.name.name(), andPostsAmount: 5))
        
        if let result = UserFetcher.fetchUserWithId(user.userId) {
            XCTAssertTrue(result.count == 1)
        }
        else {
            XCTFail()
        }
    }

    func testSaveUsers() {
        
        var arr = [User]()
        
        while arr.count < 20 {
            let user = UserCreator.create()
            arr.append(user)
        }
        
        XCTAssertTrue(UserSaver.saveUsers(arr))
        
        if let results = UserFetcher.fetchUsers() {
            XCTAssertTrue(results.count >= 20)
        }
        else {
            XCTFail()
        }
    }
}
