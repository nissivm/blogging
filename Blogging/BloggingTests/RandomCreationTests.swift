//
//  RandomCreationTests.swift
//  BloggingTests
//
//  Created by Nissi Vieira Miranda on 12/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import XCTest
import Fakery
@testable import Blogging

class RandomCreationTests: XCTestCase {
    
    var faker: Faker!
    
    override func setUp() {
        faker = Faker(locale: "")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testRandomDateCreation() {
        
        let randomDate = RandomDate.getDate()
        let dateString = DateFormatter().getFormatedDate(randomDate, forLocale: "en_US")
        XCTAssert(dateString != "")
    }
    
    func testRandomUsernameCreation() {
        
        let random = faker.internet.username(separator: "")
        XCTAssert(random != "")
    }
    
    func testRandomNameCreation() {
        
        let random = faker.name.name()
        XCTAssert(random != "")
    }
    
    func testRandomBlogTitleCreation() {
        
        var counter = 1
        while counter < 6 {
            let random = faker.lorem.sentence(wordsAmount: counter)
            XCTAssert(random != "")
            counter += 1
        }
    }
    
    func testRandomBlogBodyCreation() {
        
        var counter = 3
        while counter < 21 {
            let random = faker.lorem.paragraphs(amount: counter)
            XCTAssert(random != "")
            counter += 1
        }
    }
}
