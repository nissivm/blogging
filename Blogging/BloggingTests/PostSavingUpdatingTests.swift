//
//  PostSavingUpdatingTests.swift
//  BloggingTests
//
//  Created by Nissi Vieira Miranda on 13/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import XCTest
@testable import Blogging

class PostSavingUpdatingTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSaveUpdatePost() {
        
        let creatorId = UUID().uuidString
        let post = PostCreator.create(creatorId)
        XCTAssertTrue(PostSaver.savePost(post))
        
        if let result = PostFetcher.fetchPostWithId(post.postId) {
            XCTAssertTrue(result.count == 1)
        }
        else {
            XCTFail()
        }
        
        XCTAssertTrue(PostUpdater.updatePost(post, withTitle: RandomTitle.getTitle(), andBody: RandomBody.getBody()))
        
        if let result = PostFetcher.fetchPostWithId(post.postId) {
            XCTAssertTrue(result.count == 1)
        }
        else {
            XCTFail()
        }
    }
    
    // Tested in an iPhone 8 plus - Passed
    
    func testSaveUpdatePosts() {
        
        let total = 900
        let creatorId = UUID().uuidString
        var arr = [Post]()
        
        while arr.count < total {
            let post = PostCreator.create(creatorId)
            arr.append(post)
        }
        
        XCTAssertTrue(PostSaver.savePosts(arr))
        
        let results = PostFetcher.fetchPostsForCreator(creatorId)
        var amount = 0
        
        if let r = results {
            amount = r.count
            XCTAssertTrue(amount >= total)
        }
        else {
            XCTFail()
        }
        
        var data = [Dictionary<String, String>]()
        var counter = 0
        
        while data.count < total {
            var dic = Dictionary<String, String>()
            dic["postId"] = arr[counter].postId
            dic["title"] = RandomTitle.getTitle()
            dic["body"] = RandomBody.getBody()
            data.append(dic)
            counter += 1
        }
        
        XCTAssertTrue(PostUpdater.updatePosts(arr, with: data))
        
        let results_ = PostFetcher.fetchPostsForCreator(creatorId)
        
        if let r_ = results_ {
            XCTAssertTrue(r_.count == amount)
        }
        else {
            XCTFail()
        }
    }
}
