//
//  PostImgCell.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 15/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

class PostImgCell: UICollectionViewCell {
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var postImageView: PostImageView!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postDate: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        container.layer.cornerRadius = 10
    }
    
    func setContent(_ post: Post) {
        
        postImageView.setPostImage(post.imageUrl)
        postTitle.text = post.title
        
        postDate.text = DateFormatter().getFormatedDate(post.postDate, forLocale: DeviceLocale.get())
    }
}
