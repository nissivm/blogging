//
//  UserCell.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 14/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

class UserCell: UICollectionViewCell {
    
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var userName: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        background.layer.cornerRadius = 10
    }
}
