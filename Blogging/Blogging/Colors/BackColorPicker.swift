//
//  BackColorPicker.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 14/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import UIKit

class BackColorPicker {
    
    static private let colors = ["#3ab44a", "#49a747", "#569c44", "#639241", "#70863e", "#7b7d3c",
                                 "#887239", "#956737", "#a15d34", "#ad5332", "#b8492f", "#c33f2d",
                                 "#cf362a", "#db2c28", "#e72125"]
    
    static func getBackColor(_ posts: Int) -> UIColor {
        
        switch posts {
        case 0...60:
            return UIColor(hexString: colors[0])
        case 61...120:
            return UIColor(hexString: colors[1])
        case 121...180:
            return UIColor(hexString: colors[2])
        case 181...240:
            return UIColor(hexString: colors[3])
        case 241...300:
            return UIColor(hexString: colors[4])
        case 301...360:
            return UIColor(hexString: colors[5])
        case 361...420:
            return UIColor(hexString: colors[6])
        case 421...480:
            return UIColor(hexString: colors[7])
        case 481...540:
            return UIColor(hexString: colors[8])
        case 541...600:
            return UIColor(hexString: colors[9])
        case 601...660:
            return UIColor(hexString: colors[10])
        case 661...720:
            return UIColor(hexString: colors[11])
        case 721...780:
            return UIColor(hexString: colors[12])
        case 781...840:
            return UIColor(hexString: colors[13])
        default:
            return UIColor(hexString: colors[14])
        }
    }
}
