//
//  Constants.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 14/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import UIKit

class Constants {
    
    // Same values as the margin set for view controllers .xib files
    //static let margin_: Float = 15.0
    static let margin: CGFloat = 15.0
}
