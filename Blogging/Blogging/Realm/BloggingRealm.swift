//
//  BloggingRealm.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 13/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import RealmSwift

class BloggingRealm {
    
    static private var realm: Realm?
    
    static func getRealm() -> Realm?
    {
        if realm != nil
        {
            return realm
        }
        
        do
        {
            realm = try Realm()
            return realm
        }
        catch
        {
            return nil
        }
    }
}
