//
//  User.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 13/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    
    @objc dynamic var userId = ""
    @objc dynamic var creationDate = Date()
    @objc dynamic var username = ""
    @objc dynamic var name = ""
    @objc dynamic var postsAmount = 0
    
    override static func primaryKey() -> String? {
        return "userId"
    }
}
