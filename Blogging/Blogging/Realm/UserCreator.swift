//
//  UserCreator.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 13/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import RealmSwift
import Fakery

class UserCreator {
    
    static func create() -> User {
        
        let faker = Faker(locale: DeviceLocale.get())
        
        let user = User()
        user.userId = UUID().uuidString
        user.creationDate = RandomDate.getDate()
        user.username = faker.internet.username(separator: "")
        user.name = faker.name.name()
        user.postsAmount = RandomPostsAmount.getPostsAmount()
        
        return user
    }
}
