//
//  UserSaver.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 13/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import RealmSwift

class UserSaver {
    
    static func saveUser(_ user: User) -> Bool {
        if let realm = BloggingRealm.getRealm() {
            do
            {
                try realm.write
                {
                    realm.add(user)
                    print("\n User \(user.name) successfully saved! \n")
                }
                
                return true
            }
            catch {
                print("\n User \(user.name) NOT saved -> \(error) \n")
            }
        }
        
        return false
    }
    
    static func saveUsers(_ users: [User]) -> Bool {
        if let realm = BloggingRealm.getRealm() {
            do
            {
                try realm.write
                {
                    realm.add(users)
                    print("\n Users successfully saved! \n")
                }
                
                return true
            }
            catch {
                print("\n Users NOT saved -> \(error) \n")
            }
        }
        
        return false
    }
}
