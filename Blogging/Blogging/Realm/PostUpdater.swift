//
//  PostUpdater.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 13/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import RealmSwift

class PostUpdater {
    
    static func updatePost(_ post: Post, withTitle t: String, andBody b: String) -> Bool {
        if let realm = BloggingRealm.getRealm() {
            do
            {
                try realm.write
                {
                    post.title = t
                    post.body = b
                    realm.add(post, update: true)
                    print("\n Post '\(post.title)' successfully updated! \n")
                }
                
                return true
            }
            catch {
                print("\n Post '\(post.title)' NOT updated -> \(error) \n")
            }
        }
        
        return false
    }
    
    static func updatePosts(_ posts: [Post], with data: [Dictionary<String, String>]) -> Bool {
        if let realm = BloggingRealm.getRealm() {
            do
            {
                try realm.write
                {
                    for post in posts {
                        let filtered = data.filter { $0["postId"] == post.postId }
                        if let title = filtered[0]["title"] {
                            post.title = title
                        }
                        if let body = filtered[0]["body"] {
                            post.body = body
                        }
                    }
                    
                    realm.add(posts, update: true)
                    print("\n Posts successfully updated! \n")
                }
                
                return true
            }
            catch {
                print("\n Posts NOT updated -> \(error) \n")
            }
        }
        
        return false
    }
}
