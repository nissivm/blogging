//
//  UserFetcher.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 14/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import RealmSwift

class UserFetcher {
    
    static func fetchUserWithId(_ userId: String) -> Results<User>? {
        if let realm = BloggingRealm.getRealm() {
            let predicate = NSPredicate(format: "userId == %@", userId)
            let result = realm.objects(User.self).filter(predicate)
            if result.count > 0 {
                return result
            }
        }
        
        return nil
    }
    
    static func fetchUsers() -> Results<User>? {
        if let realm = BloggingRealm.getRealm() {
            let results = realm.objects(User.self)
            if results.count > 0 {
                return results
            }
        }
        
        return nil
    }
}
