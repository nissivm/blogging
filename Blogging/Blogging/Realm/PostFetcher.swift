//
//  PostFetcher.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 14/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import RealmSwift

class PostFetcher {
    
    static func fetchPostWithId(_ postId: String) -> Results<Post>? {
        if let realm = BloggingRealm.getRealm() {
            let predicate = NSPredicate(format: "postId == %@", postId)
            let result = realm.objects(Post.self).filter(predicate)
            if result.count > 0 {
                return result
            }
        }
        
        return nil
    }
    
    static func fetchPostsForCreator(_ creatorId: String) -> Results<Post>? {
        if let realm = BloggingRealm.getRealm() {
            let predicate = NSPredicate(format: "creatorId == %@", creatorId)
            let results = realm.objects(Post.self).filter(predicate)
            if results.count > 0 {
                return results
            }
        }
        
        return nil
    }
}
