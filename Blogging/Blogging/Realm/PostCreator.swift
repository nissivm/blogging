//
//  PostCreator.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 13/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import RealmSwift

class PostCreator {
    
    static func create(_ creatorId: String) -> Post {
        
        let post = Post()
        post.postId = UUID().uuidString
        post.creatorId = creatorId
        post.postDate = RandomDate.getDate()
        post.title = RandomTitle.getTitle()
        post.body = RandomBody.getBody()
        post.imageUrl = RandomImage.getImage()
        
        return post
    }
}
