//
//  UserUpdater.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 14/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import RealmSwift

class UserUpdater {
    
    static func updateUser(_ user: User, withName n: String, andPostsAmount p: Int) -> Bool {
        if let realm = BloggingRealm.getRealm() {
            do
            {
                try realm.write
                {
                    user.name = n
                    user.postsAmount = p
                    realm.add(user, update: true)
                    print("\n User '\(user.name)' successfully updated! \n")
                }
                
                return true
            }
            catch {
                print("\n User '\(user.name)' NOT updated -> \(error) \n")
            }
        }
        
        return false
    }
}
