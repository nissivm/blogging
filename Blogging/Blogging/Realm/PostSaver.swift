//
//  PostSaver.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 13/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import RealmSwift

class PostSaver {
    
    static func savePost(_ post: Post) -> Bool {
        if let realm = BloggingRealm.getRealm() {
            do
            {
                try realm.write
                {
                    realm.add(post)
                    print("\n Post '\(post.title)' successfully saved! \n")
                }
                
                return true
            }
            catch {
                print("\n Post '\(post.title)' NOT saved -> \(error) \n")
            }
        }
        
        return false
    }
    
    static func savePosts(_ posts: [Post]) -> Bool {
        if let realm = BloggingRealm.getRealm() {
            do
            {
                try realm.write
                {
                    realm.add(posts)
                    print("\n Posts successfully saved! \n")
                }
                
                return true
            }
            catch {
                print("\n Posts NOT saved -> \(error) \n")
            }
        }
        
        return false
    }
}
