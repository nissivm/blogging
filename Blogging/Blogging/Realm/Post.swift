//
//  Post.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 13/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import RealmSwift

class Post: Object {
    
    @objc dynamic var postId = ""
    @objc dynamic var creatorId = ""
    @objc dynamic var postDate = Date()
    @objc dynamic var title = ""
    @objc dynamic var body = ""
    @objc dynamic var imageUrl = ""
    
    override static func primaryKey() -> String? {
        return "postId"
    }
}
