//
//  NavViewControllerExt.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 15/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

extension UIViewController {
    
    func pushPostsListViewController(_ user: User, _ posts: Results<Post>, _ isCurrentUser: Bool) {
        
        guard self is UsersListViewController else {
            return
        }
        
        guard let nav = self.navigationController else {
            return
        }
        
        let vc = PostsListViewController(nibName: "PostsListViewController", bundle: nil)
        vc.user = user
        vc.posts = posts
        vc.isCurrentUser = isCurrentUser
        nav.pushViewController(vc, animated: true)
    }
    
    func pushPostDetailViewController(_ post: Post) {
        
        guard self is PostsListViewController else {
            return
        }
        
        guard let nav = self.navigationController else {
            return
        }
        
        let vc = PostDetailViewController(nibName: "PostDetailViewController", bundle: nil)
        vc.post = post
        nav.pushViewController(vc, animated: true)
    }
}
