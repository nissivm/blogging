//
//  DateFormatterExt.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 12/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    func getFormatedDate(_ date: Date, forLocale loc: String) -> String {
        
        self.dateStyle = .medium
        self.timeStyle = .none
        self.locale = Locale(identifier: loc)
        return self.string(from: date)
    }
}
