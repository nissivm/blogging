//
//  PostsListViewController.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 15/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit
import RealmSwift
import MBProgressHUD

class PostsListViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var user: User!
    var posts: Results<Post>?
    var isCurrentUser = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let back = NSLocalizedString("Back", comment: "")
        navigationItem.backBarButtonItem = UIBarButtonItem(title: back, style: .plain, target: nil, action: nil)
        navigationItem.title = user.name
        
        if isCurrentUser {
            let needsSync = NSLocalizedString("NeedsSync", comment: "")
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: needsSync, style: .plain, target: self, action: #selector(needsSynchronize))
        }
        
        collectionView.register(UINib(nibName: "PostImgCell", bundle: nil), forCellWithReuseIdentifier: "PostImgCell")
        collectionView.register(UINib(nibName: "PostCell", bundle: nil), forCellWithReuseIdentifier: "PostCell")
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(orientationDidChange),
        name: UIDevice.orientationDidChangeNotification,
        object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        
        if timer != nil {
            timer!.invalidate()
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    //--------------------------------------------------------//
    // Orientation did change Notification
    //--------------------------------------------------------//
    
    @objc func orientationDidChange() {
        collectionView.reloadData()
    }
    
    //--------------------------------------------------------//
    // Needs sync
    //--------------------------------------------------------//
    
    private var counter = 0
    private var timer: Timer?
    
    @objc func needsSynchronize() {
        
        guard posts != nil else {
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        updateBatchOfPosts()
    }
    
    @objc private func updateBatchOfPosts() {
        
        guard let posts = posts else {
            return
        }
        
        var arr = [Post]()
        var data = [Dictionary<String, String>]()
        
        while arr.count < 20 {
            
            var dic = Dictionary<String, String>()
            dic["postId"] = posts[counter].postId
            dic["title"] = RandomTitle.getTitle()
            dic["body"] = RandomBody.getBody()
            
            data.append(dic)
            
            arr.append(posts[counter])
            counter += 1
        }
        
        if PostUpdater.updatePosts(arr, with: data) {
            
            if counter < posts.count - 1 {
                
                timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(updateBatchOfPosts), userInfo: nil, repeats: false)
            }
            else {
                
                MBProgressHUD.hide(for: self.view, animated: true)
                counter = 0
                timer = nil
                
                let title = NSLocalizedString("Success", comment: "")
                let msg = NSLocalizedString("Msg4", comment: "")
                self.showOkAlert(title, msg)
            }
        }
        else {
            MBProgressHUD.hide(for: self.view, animated: true)
            counter = 0
            timer = nil
        }
    }
    
    //--------------------------------------------------------//
    // Create posts
    //--------------------------------------------------------//
    
    private func createPosts(_ postsLeft: Int) {
        
        var arr = [Post]()
        
        let total = postsLeft <= 20 ? postsLeft : 20
        
        while arr.count < total {
            let post = PostCreator.create(user.userId)
            arr.append(post)
        }
        
        var saved = false
        
        if arr.count == 1 {
            saved = PostSaver.savePost(arr[0])
        }
        else {
            saved = PostSaver.savePosts(arr)
        }
        
        if saved {
            if let p = PostFetcher.fetchPostsForCreator(user.userId) {
                posts = nil
                posts = p
            }
        }
    }
}

extension PostsListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let posts = posts {
            return posts.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let posts = posts else {
            return UICollectionViewCell()
        }
        
        if indexPath.item == posts.count - 1 {
            if posts.count < user.postsAmount {
                createPosts(user.postsAmount - posts.count)
            }
        }
        
        let post = posts[indexPath.item]
        
        if post.imageUrl != "" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostImgCell", for: indexPath) as! PostImgCell
            cell.setContent(post)
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCell", for: indexPath) as! PostCell
            cell.setContent(post)
            return cell
        }
    }
}

extension PostsListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let posts = posts else {
            return
        }
        
        let post = posts[indexPath.item]
        self.pushPostDetailViewController(post)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.margin
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAt: IndexPath) -> CGSize {
        
        var imgUrl = ""
        if let posts = posts {
            let post = posts[sizeForItemAt.item]
            imgUrl = post.imageUrl
        }
        
        let collW = collectionView.bounds.size.width
        let collH = collectionView.bounds.size.height
        
        if Device.isIphone {
            if collH > collW {
                let w = collW
                var h = collW
                if imgUrl == "" {
                    h = w/2
                }
                return CGSize(width: w, height: h)
            }
            else {
                return CGSize(width: (collW - Constants.margin)/2,
                              height: (collW - Constants.margin)/2)
            }
        }
        else { // iPad
            if collH > collW {
                return CGSize(width: (collW - Constants.margin)/2,
                              height: (collW - Constants.margin)/2)
            }
            else {
                return CGSize(width: (collW - (Constants.margin * 2))/3,
                              height: (collW - (Constants.margin * 2))/3)
            }
        }
    }
}
