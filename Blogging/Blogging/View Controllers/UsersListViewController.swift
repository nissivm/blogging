//
//  UsersListViewController.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 14/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit
import RealmSwift

class UsersListViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    private var users: Results<User>?
    private var currentUser: User?
    private var rightBarButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        var buttonTitle = NSLocalizedString("Register", comment: "")
        if let current = CurrentUser.get() {
            currentUser = current
            buttonTitle = NSLocalizedString("MyPosts", comment: "")
        }
        
        rightBarButton = UIBarButtonItem(title: buttonTitle, style: .plain, target: self, action: #selector(currentUserButtonTapped))
        navigationItem.rightBarButtonItem = rightBarButton
        
        collectionView.register(UINib(nibName: "UserCell", bundle: nil), forCellWithReuseIdentifier: "UserCell")
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(orientationDidChange),
        name: UIDevice.orientationDidChangeNotification,
        object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if let usrs = UserFetcher.fetchUsers() {
            
            users = usrs
            
            if usrs.count < 60 {
                createUsers()
            }
        }
        else {
            createUsers()
        }
    }
    
    //--------------------------------------------------------//
    // Current user button tapped
    //--------------------------------------------------------//
    
    @objc func currentUserButtonTapped() {
        
        if let current = currentUser {
            if let posts = PostFetcher.fetchPostsForCreator(current.userId) {
                self.pushPostsListViewController(current, posts, true)
            }
            else if let posts = createPosts(current) {
                self.pushPostsListViewController(current, posts, true)
            }
            else {
                let title = NSLocalizedString("NoPosts", comment: "")
                let msg = NSLocalizedString("Msg1", comment: "")
                self.showOkAlert(title, msg)
            }
        }
        else if let c = CurrentUser.get() {
            currentUser = c
            let title = NSLocalizedString("MyPosts", comment: "")
            rightBarButton = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(currentUserButtonTapped))
            navigationItem.rightBarButtonItem = rightBarButton
        }
    }
    
    //--------------------------------------------------------//
    // Orientation did change Notification
    //--------------------------------------------------------//
    
    @objc func orientationDidChange() {
        collectionView.reloadData()
    }
    
    //--------------------------------------------------------//
    // Create 20 new users
    //--------------------------------------------------------//
    
    private func createUsers() {
        
        var arr = [User]()
        
        while arr.count < 20 {
            let user = UserCreator.create()
            arr.append(user)
        }
        
        if UserSaver.saveUsers(arr) {
            if let usrs = UserFetcher.fetchUsers() {
                
                if users == nil {
                    users = usrs
                }
                else {
                    users = usrs
                    collectionView.reloadData()
                }
            }
        }
    }
    
    //--------------------------------------------------------//
    // Create posts
    //--------------------------------------------------------//
    
    private func createPosts(_ user: User) -> Results<Post>? {
        
        var arr = [Post]()
        
        let total = user.postsAmount <= 20 ? user.postsAmount : 20
        
        while arr.count < total {
            let post = PostCreator.create(user.userId)
            arr.append(post)
        }
        
        var saved = false
        
        if arr.count == 1 {
            saved = PostSaver.savePost(arr[0])
        }
        else {
            saved = PostSaver.savePosts(arr)
        }
        
        if saved {
            return PostFetcher.fetchPostsForCreator(user.userId)
        }
        
        return nil
    }
}

extension UsersListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let u = users {
            return u.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let u = users else {
            return UICollectionViewCell()
        }
        
        let user = u[indexPath.item]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserCell", for: indexPath) as! UserCell
        cell.background.backgroundColor = BackColorPicker.getBackColor(user.postsAmount)
        cell.userName.text = user.name
        
        if indexPath.item == u.count - 1 {
            if u.count < 60 {
                createUsers()
            }
        }
        
        return cell
    }
}

extension UsersListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let u = users else {
            return
        }
        
        let user = u[indexPath.item]
        
        if user.postsAmount == 0 {
            let title = NSLocalizedString("NoPosts", comment: "")
            let msg = NSLocalizedString("Msg2", comment: "")
            self.showOkAlert(title, msg)
            return
        }
        
        if let posts = PostFetcher.fetchPostsForCreator(user.userId) {
            self.pushPostsListViewController(user, posts, false)
        }
        else if let posts = createPosts(user) {
            self.pushPostsListViewController(user, posts, false)
        }
        else {
            let title = NSLocalizedString("NoPosts", comment: "")
            let msg = NSLocalizedString("Msg3", comment: "")
            self.showOkAlert(title, msg)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.margin
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAt: IndexPath) -> CGSize {
        
        let collW = collectionView.bounds.size.width
        let collH = collectionView.bounds.size.height
        
        if Device.isIphone {
            if collH > collW {
                return CGSize(width: (collW - Constants.margin)/2,
                              height: (collW - Constants.margin)/2)
            }
            else {
                return CGSize(width: (collW - (Constants.margin * 2))/3,
                              height: (collW - (Constants.margin * 2))/3)
            }
        }
        else { // iPad
            if collH > collW {
                return CGSize(width: (collW - (Constants.margin * 2))/3,
                              height: (collW - (Constants.margin * 2))/3)
            }
            else {
                return CGSize(width: (collW - (Constants.margin * 3))/4,
                              height: (collW - (Constants.margin * 3))/4)
            }
        }
    }
}
