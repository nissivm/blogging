//
//  PostDetailViewController.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 16/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

class PostDetailViewController: UIViewController {
    
    @IBOutlet weak var postImageView: PostImageView!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postBody: UITextView!
    @IBOutlet weak var postDate: UILabel!
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    
    var post: Post!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(orientationDidChange),
        name: UIDevice.orientationDidChangeNotification,
        object: nil)
        
        let screenSize = UIScreen.main.bounds.size
        containerHeightConstraint.constant = screenSize.height * 0.4
        
        let back = NSLocalizedString("Back", comment: "")
        navigationItem.backBarButtonItem = UIBarButtonItem(title: back, style: .plain, target: nil, action: nil)
        
        if post.imageUrl != "" {
            postImageView.setPostImage(post.imageUrl)
        }
        
        postTitle.text = post.title
        postBody.text = post.body
        postDate.text = DateFormatter().getFormatedDate(post.postDate, forLocale: DeviceLocale.get())
    }
    
    //--------------------------------------------------------//
    // Orientation did change Notification
    //--------------------------------------------------------//
    
    @objc func orientationDidChange() {
        
        let screenSize = UIScreen.main.bounds.size
        var newConstant: CGFloat = 0
        
        if screenSize.height > screenSize.width {
            newConstant = screenSize.height * 0.4
        }
        else {
            newConstant = screenSize.height * 0.4
        }
        
        containerHeightConstraint.constant = newConstant
        self.view.setNeedsDisplay()
    }
}
