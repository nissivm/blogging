//
//  RandomImage.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 13/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

class RandomImage {
    
    private static let images = [
        "https://www.shoresandislands.com/SiteMedia/images/websitedata/publications/landscape/13949_3172.jpg",
        "https://hudsonvalleypress.com/wp-content/uploads/2018/03/Hot-air-baloon-festival.jpg",
        "https://palmerpawprint.org/wp-content/uploads/2019/01/anxiety-blankets-for-pets.jpg",
        "https://www.feriasbrasil.com.br/fotosfb/076390940-XG.jpg",
        "https://o-tuga.com/wp-content/uploads/2018/07/Porto-Portugal-o-tuga.jpg"]
    
    static func getImage() -> String {
        let random = Int.random(in: -3...4)
        if random >= 0 {
            return images[random]
        }
        return ""
    }
}
