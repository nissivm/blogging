//
//  RandomPostsAmount.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 14/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

class RandomPostsAmount {
    static func getPostsAmount() -> Int {
        return Int.random(in: 0...900)
    }
}
