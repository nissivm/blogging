//
//  RandomDate.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 13/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

class RandomDate {
    
    static func getDate() -> Date {
        let year: Double = 31536000 // Year in seconds
        let random = Double.random(in: -5.0...0.0)
        return Date(timeIntervalSinceNow: year*random)
    }
}
