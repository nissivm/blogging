//
//  RandomBody.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 13/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import Fakery

class RandomBody {
    
    static func getBody() -> String {
        let random = Int.random(in: 3...21)
        return Faker(locale: DeviceLocale.get()).lorem.paragraphs(amount: random)
    }
}
