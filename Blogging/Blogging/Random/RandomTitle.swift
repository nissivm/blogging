//
//  RandomTitle.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 13/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import Fakery

class RandomTitle {
    
    static func getTitle() -> String {
        let random = Int.random(in: 1...6)
        return Faker(locale: DeviceLocale.get()).lorem.sentence(wordsAmount: random)
    }
}
