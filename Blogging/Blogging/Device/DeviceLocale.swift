//
//  DeviceLocale.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 16/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

class DeviceLocale {
    
    static func get() -> String {
        var locale = "en_US"
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            locale = countryCode
        }
        return locale
    }
}
