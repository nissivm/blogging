//
//  Device.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 14/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import UIKit

class Device {
    
    static var isIphone: Bool {
        get {
            return UIScreen.main.traitCollection.userInterfaceIdiom == .phone
        }
    }
    
    static var isIpad: Bool {
        get {
            return UIScreen.main.traitCollection.userInterfaceIdiom == .pad
        }
    }
    
    static var isPortrait: Bool {
        get {
            return UIDevice.current.orientation.isPortrait
        }
    }
    
    static var isLandscape: Bool {
        get {
            return UIDevice.current.orientation.isLandscape
        }
    }
    
    static var isFaceUpDown: Bool {
        get {
            return UIDevice.current.orientation.isFlat
        }
    }
    
    static var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}
