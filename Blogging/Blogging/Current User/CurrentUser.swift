//
//  CurrentUser.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 17/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

class CurrentUser {
    
    static private var current: User?
    static private let key = "CurrentUserId"
    
    static func get() -> User? {
        
        if current == nil {
            let defaults = UserDefaults.standard
            if let userId = defaults.object(forKey: key) as? String {
                if let r = UserFetcher.fetchUserWithId(userId) {
                    current = r[0]
                }
            }
            else {
                let user = UserCreator.create()
                if user.postsAmount == 0 {
                    user.postsAmount = 500
                }
                if UserSaver.saveUser(user) {
                    if let r = UserFetcher.fetchUserWithId(user.userId) {
                        defaults.setValue(user.userId, forKey: key)
                        current = r[0]
                    }
                }
            }
        }
        
        return current
    }
}
