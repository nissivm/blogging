//
//  ImageCache.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 14/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import UIKit

class ImageCache {
    
    static private let imageCache = NSCache<NSString, UIImage>()
    
    static func getCachedImage(_ imgUrl: String) -> UIImage? {
        
        if let cachedImg = imageCache.object(forKey: NSString(string: imgUrl)) {
            return cachedImg
        }
        
        return nil
    }
    
    static func cacheImage(_ image: UIImage, _ imgUrl: String) {
        imageCache.setObject(image, forKey: NSString(string: imgUrl))
    }
}
