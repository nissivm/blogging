//
//  PostImageView.swift
//  Blogging
//
//  Created by Nissi Vieira Miranda on 15/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

class PostImageView: UIImageView {

    func setPostImage(_ stringUrl: String) {
        
        if let cachedImage = ImageCache.getCachedImage(stringUrl) {
            self.image = cachedImage
            return
        }
        
        if let url = URL(string: stringUrl) {
            loadHeroImage(url: url)
        }
    }
    
    private func loadHeroImage(url: URL) {
        
        DispatchQueue.global(qos: .background).async {
            
            guard let data = try? Data(contentsOf: url) else {
                return
            }
            
            guard let image = UIImage(data: data) else {
                return
            }
            
            DispatchQueue.main.async {
                ImageCache.cacheImage(image, url.absoluteString)
                self.image = image
            }
        }
    }
}
